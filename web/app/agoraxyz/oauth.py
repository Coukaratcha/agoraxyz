from flask import flash, current_app
from flask_login import current_user, login_user
from flask_dance.contrib.twitter import make_twitter_blueprint
from flask_dance.consumer import oauth_authorized, oauth_error
from flask_dance.consumer.storage.sqla import SQLAlchemyStorage
from sqlalchemy.orm.exc import NoResultFound
from .db import db
from .models import User, OAuth

blueprint = make_twitter_blueprint(
	storage = SQLAlchemyStorage(OAuth, db.session, user=current_user)
)

@oauth_authorized.connect_via(blueprint)
def twitter_logged_in(blueprint, token):
	if not token:
		flash("Failed to log in.", category="error")
		return False
	
	resp = blueprint.session.get("account/verify_credentials.json")
	if not resp.ok:
		flash("Failed to fetch user info.", category="error")
		return False
	
	info = resp.json()
	user_id = info["id_str"]

	query = OAuth.query.filter_by(
		provider=blueprint.name,
		provider_user_id=user_id
	)

	try:
		oauth = query.one()
	except NoResultFound:
		oauth = OAuth(
			provider=blueprint.name,
			provider_user_id=user_id,
			token=token
		)
	
	if oauth.user:
		if not oauth.user.is_active:
			flash("Your account has been disabled.", category="error")
			return False
		login_user(oauth.user)
		flash("Successfully signed in.")
	else:
		user = User(
			name=info["screen_name"],
			is_admin=info["screen_name"] == current_app.config["ADMIN"]
		)
		oauth.user = user
		db.session.add_all([user, oauth])
		db.session.commit()

		login_user(user)
		flash("Successfully signed in.")
	return False

@oauth_error.connect_via(blueprint)
def twitter_error(blueprint, message, response):
	msg = (
		"OAuth error from {name}! "
		"message={message} response={response}"
	).format(
		name=blueprint.name,
		message=message,
		response=response
	)
	flash(msg, category="error")