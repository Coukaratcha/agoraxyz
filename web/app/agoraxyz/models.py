from agoraxyz.db import db
from flask_login import LoginManager, UserMixin
from flask_dance.consumer.storage.sqla import OAuthConsumerMixin, SQLAlchemyStorage

class User(UserMixin, db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(255), unique=True, nullable=False)
	is_admin = db.Column(db.Boolean, nullable=False, default=False)
	is_moderator = db.Column(db.Boolean, nullable=False, default=False)
	is_active = db.Column(db.Boolean, nullable=False, default=True)

class OAuth(OAuthConsumerMixin, db.Model):
	provider_user_id = db.Column(db.String(255), unique=True, nullable=False)
	user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
	user = db.relationship(User)

login_manager = LoginManager()
login_manager.login_view = "twitter.login"

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

class Message(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	text = db.Column(db.Text(140), unique=True, nullable=False)
	author = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)

	@property
	def json(self):
		return {
			"id": self.id,
			"text": self.text
		}

class Vote(db.Model):
	user_id = db.Column(db.Integer, db.ForeignKey(User.id), primary_key=True)
	message_id = db.Column(db.Integer, db.ForeignKey(Message.id), primary_key=True)

class Archive(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	text = db.Column(db.Text(140), nullable=False)
	author = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)