from flask import (
	Flask,
	session,
	redirect,
	url_for,
	render_template,
	jsonify,
	flash,
	current_app,
	request,
	abort
)
from flask_login import login_required, logout_user, current_user 
from flask_socketio import emit
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import desc
import tweepy
import datetime

from .config import Config
from .db import db
from .models import login_manager, Message, Vote, User, Archive
from .oauth import blueprint
from .admin import admin
from .socketio import socketio
from .tools import awaken

app = Flask(__name__)
app.config.from_object(Config)
app.register_blueprint(blueprint, url_prefix="/login")
app.register_blueprint(admin)
db.init_app(app)
socketio.init_app(app, cors_allowed_origins=app.config["ORIGIN_URL"])
login_manager.init_app(app)

auth = tweepy.OAuthHandler(app.config["TWITTER_OAUTH_SERVER_KEY"], app.config["TWITTER_OAUTH_SERVER_SECRET"])
auth.set_access_token(app.config["TWITTER_OAUTH_SERVER_ACCESS_TOKEN_KEY"], app.config["TWITTER_OAUTH_SERVER_ACCESS_TOKEN_SECRET"])
api = tweepy.API(auth)

app.config["SLEEP"] = False

with app.app_context():
	db.create_all()

@app.route("/")
def home():
	messages = Vote.query.with_entities(Message.id.label("id"), Message.text.label("text"), db.func.count(Vote.user_id).label("score")).join(User, User.id == Vote.user_id).join(Message, Message.id == Vote.message_id).group_by(Message.id).order_by(desc("score"), Message.id).all()
	votes = []

	if current_user.is_authenticated:
		votes = Vote.query.with_entities(Vote.message_id.label("id")).filter_by(user_id=current_user.id).all()

	messages = [ {
		"id": x.id,
		"text": x.text,
		"score": x.score
	} for x in messages]

	votes = [x.id for x in votes]

	return render_template("index.html", messages = messages, votes = votes, sleep = current_app.config["SLEEP"])

@app.route("/logout")
@login_required
def logout():
	logout_user()
	flash("You have logged out")
	return redirect(url_for("home"))

@app.route("/publish", methods=["GET"])
@awaken
def publish():
	if request.args.get('secret_key') != current_app.config["SECRET_KEY"]:
		abort(403)
	
	m = Vote.query.with_entities(Vote.message_id.label("id"), Message.author.label("author"), Message.text.label("text"), db.func.count(Vote.user_id).label("score")).join(Message, Message.id==Vote.message_id).group_by(Vote.message_id).order_by(desc("score"), Message.id).first()
	s = ""
	if m:
		s = m.text
		a = Archive(
			text=m.text,
			author=m.author
		) 
		db.session.add(a)
		if current_app.config["ENABLE_PUBLISHING"]:
			api.update_status(s)

	socketio.emit("election", s, broadcast=True)
	socketio.emit("messages", [], broadcast=True)
	socketio.emit("votes", [], broadcast=True)

	Message.query.delete()
	Vote.query.delete()
	db.session.commit()
	return "{d} -- Tweet: {t}\n".format(d=datetime.datetime.now(), t=s)

@socketio.on("message")
@login_required
@awaken
def handler_message(data):
	if not current_user.is_active:
		return current_app.login_manager.unauthorized()
	
	if not data:
		return False

	query = Message.query.filter_by(
		text=data
	)

	try:
		m = query.one()
	except NoResultFound:
		m = Message(
			text=data,
			author=current_user.id
		)
		db.session.add(m)
		db.session.flush()
	
	query = Vote.query.filter_by(
		user_id=current_user.id,
		message_id=m.id
	)

	try:
		v = query.one()
	except NoResultFound:
		v = Vote(
			user_id=current_user.id,
			message_id=m.id
		)
		db.session.add(v)
		db.session.commit()

	messages = Vote.query.with_entities(Message.id.label("id"), Message.text.label("text"), db.func.count(Vote.user_id).label("score")).join(User, User.id == Vote.user_id).join(Message, Message.id == Vote.message_id).group_by(Message.id).order_by(desc("score"), Message.id).all()
	votes = Vote.query.with_entities(Vote.message_id.label("id")).filter_by(user_id=current_user.id).all()

	messages = [ {
		"id": x.id,
		"text": x.text,
		"score": x.score
	} for x in messages]

	votes = [x.id for x in votes]

	emit('votes', votes)
	emit('messages', messages, broadcast=True)

@socketio.on("vote")
@login_required
@awaken
def handler_vote(message_id):
	if not current_user.is_active:
		return current_app.login_manager.unauthorized()

	query = Vote.query.filter_by(
		user_id=current_user.id,
		message_id=message_id
	)

	try:
		v = query.one()
		db.session.delete(v)
	except NoResultFound:
		v = Vote(
			user_id=current_user.id,
			message_id=message_id
		)
		db.session.add(v)

	db.session.commit()

	messages = Vote.query.with_entities(Message.id.label("id"), Message.text.label("text"), db.func.count(Vote.user_id).label("score")).join(User, User.id == Vote.user_id).join(Message, Message.id == Vote.message_id).group_by(Message.id).order_by(desc("score"), Message.id).all()
	votes = Vote.query.with_entities(Vote.message_id.label("id")).filter_by(user_id=current_user.id).all()

	messages = [ {
		"id": x.id,
		"text": x.text,
		"score": x.score
	} for x in messages]

	votes = [x.id for x in votes]

	emit('votes', votes)
	emit('messages', messages, broadcast=True)
