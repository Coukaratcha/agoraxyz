from functools import wraps
from flask import (
	current_app
)
from flask_login import current_user

def awaken(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if current_app.config["SLEEP"]:
			return False
		return f(*args, **kwargs)
	return decorated_function

def require_admin(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if not current_user.is_admin:
			abort(401)
		return f(*args, **kwargs)
	return decorated_function

def require_moderator(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if not (current_user.is_admin or current_user.is_moderator):
			abort(401)
		return f(*args, **kwargs)
	return decorated_function