from flask import (
	Blueprint,
	abort,
	render_template,
	redirect,
	url_for,
	current_app
)
from flask_login import login_required, current_user
from flask_socketio import SocketIO
from sqlalchemy import desc
from sqlalchemy.orm import aliased
from sqlalchemy.orm.exc import NoResultFound

from .db import db
from .models import User, Message, Vote, Archive
from .socketio import socketio
from .tools import require_admin, require_moderator

admin = Blueprint("admin", __name__, template_folder="templates/admin", url_prefix="/admin")

@admin.route("/sleep")
@require_admin
def sleep():
	current_app.config["SLEEP"] = True
	return "Sleeping"

@admin.route("/wake")
@require_admin
def wake():
	current_app.config["SLEEP"] = False
	return "Awaken"

@admin.route("messages")
@require_moderator
def admin_messages():
	Author = aliased(User)

	messages = Vote.query.with_entities(Message.id.label("id"), Message.text.label("text"), Author.id.label("author_id"), Author.name.label("author_name"), db.func.count(Vote.user_id).label("score")).join(User, User.id == Vote.user_id).join(Message, Message.id == Vote.message_id).join(Author, Author.id == Message.author).group_by(Message.id).order_by(desc("score")).all()

	messages = [ {
		"id": x.id,
		"text": x.text,
		"author": {
			"id": x.author_id,
			"name": x.author_name
		},
		"score": x.score,
		"votes": []
	} for x in messages]

	for m in messages:
		votes = Vote.query.with_entities(Vote.user_id.label("id"), User.name.label("name")).join(User, User.id == Vote.user_id).filter(Vote.message_id == m["id"]).all()

		m["votes"] = [{
			"id": v.id,
			"name": v.name
		} for v in votes]

	return render_template("messages.html", messages = messages)

@admin.route("users")
@require_moderator
def admin_users():
	users = User.query.all()	
	return render_template("users.html", users = users)

@admin.route("archives")
@require_moderator
def admin_archives():
	archives = Archive.query.with_entities(Archive.id.label("id"), Archive.text.label("text"), Archive.author.label("author_id"), User.name.label("author_name")).join(User, User.id == Archive.author).order_by(Archive.id.desc()).all()

	archives = [{
		"id": x.id,
		"text": x.text,
		"author": {
			"id": x.author_id,
			"name": x.author_name
		}
	} for x in archives]

	return render_template("archives.html", messages = archives)

@admin.route("users/<string:user_name>")
@require_moderator
def admin_users_name(user_name):
	query = User.query.filter(User.name == user_name)
	try:
		u = query.one()
	except NoResultFound:
		return redirect(url_for("admin.admin_users")) 

	return render_template("users.html", user = u)

@admin.route("/grant/<string:user_name>")
@require_admin
def admin_grant_user_name(user_name):
	query = User.query.filter(User.name == user_name)
	try:
		u = query.one()
	except NoResultFound:
		return redirect(url_for("admin.admin_users"))

	u.is_admin = True
	u.is_moderator = False
	db.session.commit()
	return redirect("/admin/users/{user_name}".format(user_name=u.name))

@admin.route("/revoke/<string:user_name>")
@require_admin
def admin_revoke_user_name(user_name):
	query = User.query.filter(User.name == user_name)
	try:
		u = query.one()
	except NoResultFound:
		return redirect(url_for("admin.admin_users"))

	u.is_admin = False
	db.session.commit()
	return redirect("/admin/users/{user_name}".format(user_name=u.name))

@admin.route("/grant_mod/<string:user_name>")
@require_admin
def admin_grant_mod_user_name(user_name):
	query = User.query.filter(User.name == user_name)
	try:
		u = query.one()
	except NoResultFound:
		return redirect(url_for("admin.admin_users"))

	u.is_moderator = True
	u.is_admin = False
	db.session.commit()
	return redirect("/admin/users/{user_name}".format(user_name=u.name))

@admin.route("/revoke_mod/<string:user_name>")
@require_admin
def admin_revoke_mod_user_name(user_name):
	query = User.query.filter(User.name == user_name)
	try:
		u = query.one()
	except NoResultFound:
		return redirect(url_for("admin.admin_users"))

	u.is_moderator = False
	db.session.commit()
	return redirect("/admin/users/{user_name}".format(user_name=u.name))

@admin.route("/ban/<string:user_name>")
@require_moderator
def admin_ban_user_name(user_name):
	query = User.query.filter((User.is_admin == False) & (User.is_moderator == False) & (User.name == user_name))
	try:
		u = query.one()
	except NoResultFound:
		return redirect(url_for("admin.admin_users"))

	u.is_active = False
	Vote.query.filter(Vote.user_id == u.id).delete()
	db.session.commit()
	
	messages = Vote.query.with_entities(Message.id.label("id"), Message.text.label("text"), db.func.count(Vote.user_id).label("score")).join(User, User.id == Vote.user_id).join(Message, Message.id == Vote.message_id).group_by(Message.id).order_by(desc("score")).all()

	messages = [ {
		"id": x.id,
		"text": x.text,
		"score": x.score
	} for x in messages]

	socketio.emit('messages', messages, broadcast=True)

	return redirect("/admin/users/{user_name}".format(user_name=u.name))

@admin.route("/unban/<string:user_name>")
@require_moderator
def admin_unban_user_name(user_name):
	query = User.query.filter(User.name == user_name)
	try:
		u = query.one()
	except NoResultFound:
		return redirect(url_for("admin.admin_users"))

	u.is_active = True
	db.session.commit()
	return redirect("/admin/users/{user_name}".format(user_name=u.name))

@admin.route("/delete/message/<int:msg_id>")
@require_moderator
def admin_delete_message(msg_id):
	try:
		m = Message.query.get(msg_id)
	except NoResultFound:
		return redirect(url_for("admin.admin_messages"))
	
	db.session.delete(m)
	Vote.query.filter(Vote.message_id == msg_id).delete()
	db.session.commit()
	
	messages = Vote.query.with_entities(Message.id.label("id"), Message.text.label("text"), db.func.count(Vote.user_id).label("score")).join(User, User.id == Vote.user_id).join(Message, Message.id == Vote.message_id).group_by(Message.id).order_by(desc("score")).all()

	messages = [ {
		"id": x.id,
		"text": x.text,
		"score": x.score
	} for x in messages]

	socketio.emit('messages', messages, broadcast=True)

	return redirect(url_for("admin.admin_messages"))

@admin.route("/delete/message_ban/<int:msg_id>")
@require_moderator
def admin_delete_message_ban(msg_id):
	try:
		m = Message.query.get(msg_id)
	except NoResultFound:
		return redirect(url_for("admin.admin_messages"))
	
	try:
		u = User.query.filter((User.is_admin == False) & (User.is_moderator == False) & (User.id == m.author)).one()
		u.is_active = False
		Vote.query.filter(Vote.user_id == u.id).delete()
	except NoResultFound:
		pass
	
	Vote.query.filter(Vote.message_id == msg_id).delete()
	db.session.delete(m)
	db.session.commit()
	
	messages = Vote.query.with_entities(Message.id.label("id"), Message.text.label("text"), db.func.count(Vote.user_id).label("score")).join(User, User.id == Vote.user_id).join(Message, Message.id == Vote.message_id).group_by(Message.id).order_by(desc("score")).all()

	messages = [ {
		"id": x.id,
		"text": x.text,
		"score": x.score
	} for x in messages]

	socketio.emit('messages', messages, broadcast=True)

	return redirect(url_for("admin.admin_messages"))

@admin.route("/delete/messsage_ban_all/<int:msg_id>")
@require_moderator
def admin_delete_message_ban_all(msg_id):
	try:
		m = Message.query.get(msg_id)
	except NoResultFound:
		return redirect(url_for("admin.admin_messages"))

	users = User.query.join(Vote, Vote.user_id == User.id).filter((Vote.message_id == msg_id) & (User.is_admin == False) & (User.is_moderator == False)).all()
	for u in users:
		u.is_active = False
	Vote.query.filter((Vote.message_id == msg_id) | (Vote.user_id.in_([u.id for u in users]))).delete(synchronize_session="fetch")
	db.session.delete(m)
	db.session.commit()

	messages = Vote.query.with_entities(Message.id.label("id"), Message.text.label("text"), db.func.count(Vote.user_id).label("score")).join(User, User.id == Vote.user_id).join(Message, Message.id == Vote.message_id).group_by(Message.id).order_by(desc("score")).all()

	messages = [ {
		"id": x.id,
		"text": x.text,
		"score": x.score
	} for x in messages]

	socketio.emit('messages', messages, broadcast=True)

	return redirect(url_for("admin.admin_messages"))