let socket = io();
let votes = [];

socket.on("connect", function() {
	if (form){
		form.addEventListener("keydown", function(e){
			if (e.ctrlKey && e.code == "Enter"){
				submit(this);
			}
		});
		form.addEventListener("submit", function(e){
			e.preventDefault();
			submit(this);
		});

		const buttons = document.querySelectorAll("#messages li button");

		for (b of buttons){
			b.addEventListener("click", vote);
		}
	}

	socket.on("messages", update);
	socket.on("votes", updateVotes);
	socket.on("election", election);
});

const form = document.querySelector("form");

if (form){
	const textarea = form.querySelector("textarea");

	textarea.addEventListener("input", function(){
		const small = document.querySelector("form small");
		const button = document.querySelector("form button");
		
		small.textContent = 140 - this.value.length;

		if (140 - this.value.length < 0){
			small.classList.add("text-danger");
			this.classList.add("is-invalid");
			button.setAttribute("disabled", true);
		}
		else{
			small.classList.remove("text-danger");
			this.classList.remove("is-invalid");
			button.removeAttribute("disabled");
		}
	});

	votes = Array.from(document.querySelectorAll("#messages li.upvoted")).map(li => parseInt(li.getAttribute("data-id")));
}

function update(data){
	const messages = data;
	const list = document.querySelector("#messages");

	list.innerHTML = "";

	for (m of messages){
		const li = document.createElement("li");
		li.className = "list-group-item d-flex justify-content-between align-items-center";
		li.setAttribute("data-id", m.id);
		const isVoted = votes.indexOf(parseInt(m.id)) != -1;

		if (isVoted){
			li.classList.add("upvoted");
		}

		const div = document.createElement("div");
		const button = document.createElement("button");
		button.className = "btn btn-sm font-monospace fw-bold me-2";
		button.classList.add(isVoted ? "btn-danger" : "btn-success");
		button.innerHTML = isVoted ? "&minus;" : "&plus;";
		
		div.appendChild(button);
		div.appendChild(document.createTextNode(m.text));

		li.appendChild(div);

		const badge = document.createElement("span");
		badge.className = "badge bg-primary rounded-pill";
		badge.innerText = m.score;
		li.appendChild(badge);

		list.appendChild(li);
	}

	const buttons = document.querySelectorAll("#messages li button");

	for (b of buttons){
		b.addEventListener("click", vote);
	}
}

function updateVotes(data){
	votes = data;

	document.querySelectorAll("#messages li").forEach(function(e){
		const e_id = parseInt(e.getAttribute("data-id"));
		const isVoted = votes.indexOf(e_id) != -1;

		if (e.classList.contains("upvoted") && !isVoted){
			e.classList.remove("upvoted");
			const button = e.querySelector("div > button");
			button.classList.remove("btn-danger");
			button.classList.add("btn-success");
			button.innerHTML = "&plus;";
		}
		else if (!e.classList.contains("upvoted") && isVoted){
			e.classList.add("upvoted");
			const button = e.querySelector("div > button");
			button.classList.add("btn-danger");
			button.classList.remove("btn-success");
			button.innerHTML= "&minus;";
		}
	});
}

function vote(){
	const id = parseInt(this.parentNode.parentNode.getAttribute("data-id"));
	socket.emit('vote', id);
}

function election(data){
	tweetAlert(data);
}

function tweetAlert(data){
	const text = data.length > 0 ? `Le tweet suivant a été posté ! '${data}'` : "Aucun tweet n'a été posté...";
	showAlert("success", text)
}

function showAlert(type, text){
	const alert = document.createElement("div");
	alert.className = `alert alert-${type} alert-dismissible fade show`;
	alert.setAttribute("role", "alert");

	alert.appendChild(document.createTextNode(text));

	const button = document.createElement("button");
	button.setAttribute("type", "button");
	button.setAttribute("data-bs-dismiss", "alert");
	button.setAttribute("aria-label", "Close");
	button.className = "btn-close";

	alert.appendChild(button);
	const flash = document.querySelector(".flash");
	flash.appendChild(alert);
}

function submit(form){
	const textarea = form.querySelector("textarea");
	const small = form.querySelector("small");

	if (textarea.value.length > 0 && textarea.value.length <= 140){
		socket.emit("message", textarea.value);
		textarea.value = "";
		small.innerText = 140;
	}
}

function updateCountdown(){
	const current = new Date();
	const start = new Date(current.getTime());
	const start_minutes = (Math.floor(start.getMinutes() / 15)) * 15;
	start.setMinutes(start_minutes);
	start.setSeconds(0);
	const target = new Date(start.getTime() + 15*60_000);
	const countdown = (target.getTime() - current.getTime()) / 1000;
	const string_countdown = `${Math.floor(countdown / 60)}:${("0" + countdown % 60).slice(-2)}`;

	const span = document.querySelector("#timer");
	span.textContent = string_countdown;
}

let timer = setInterval(updateCountdown, 500);